// Synchronous task
	// promise 1 : cook
		// promise 2 : eat
			// promise 3 : wash dishes

// Asynchronous task
	// promise 1 : papayat ako
		// promise 2 : magkape sa antipolo

/*
	What is API
	- Application Interface Programming
	- https://jsonplaceholder.typicode.com/posts - API
	- I access natin xa, ang triky is only xa at need natin iaccess saating local repository
	What is it for?
	- Hides the server beneath an interface layer
		- Hidden complexity makes apps easier to use
		- Set the rule of interaction between front and backends of an application improving security
	What is REST
	- REpresentational
	- An architectural style
	What problem does it solve?
	- The need to separate user interface concerns of the client from data storage concerns of the server
	What does it solve the problema
	- Statelessness
		- Server does not need to know client state and vice versa
		- Every client request has all the information it needs to be processed by the API even not 
		knowing the server state
		- Made possible via resources and HTTP methods - GET POST DELETE PUT
	- Standarilized communication
		- Enable the server to understand processs and respond to client request without knowing client state
		- Implement via resources represent as Uniform Resource Identified "URI" endpoints (ex./users) and HTTP methods, meaning it is much easuer to understand
	- Anatomy of client request
		- Operation to be performed dictated by HTTP method
		REST API METHOD
		- GET = Received information about an API resources
		- POST = Create an API resources
		- PUT = Update an API resources
		- DELETE = Delete an API resources
*/

console.log("Hello day 33!");

// >> Synchronous proof
// JavaScript is by default synchronous, meaning that it only executes on statement at a time
/*
	console.log("Hello World p.2");
	conosle.log("Hello again"); // since nag error itong part na to, hindi na tutuloy sa susunod na part
	console.log("Good bye");
*/
// Code blocking - waiting for the specific statement to finish before executing the next statement

// by default JS is synchronous as it needs to complete the for loop before it show the console message "Hello again"
for(let i = 0; i <= 5; i++){
	console.log(i);
}
console.log("Hello again");

// Asynchronous means that we can proceed to execute other statement, while time consuming code is running in the background

// The fetch API that allows us to asynchronous request for a resource (data)
// "fetch()" method in JavaScript is used to request to the server and load information on the webpage
// Syntax:
	// fetch("apiURL");

console.log("-------------------------------------------");
// [SECTION] Getting all post
fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => console.log(res.json())); // kinuha natin ang mga json nya and kinonvert natin sa javascript
// arrow function with parameter res
// "res" is the paramater of the function
// The ".then()" method captures the response object and returns a promise which will be f"ullfilled" or "rejected"
// .json() identifies a network response and converts it directly to JavaScript
// stringify and .parse - we can access it because we are using local however this is different as we are using network or api's

fetch("https://jsonplaceholder.typicode.com/posts")
// .then(res => console.log(res));
.then(res => console.log(res.status));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json()) // returns a response that is converted into a JavaScript object
.then(response => console.log(response)); //finetch natin ang document from the api then kinonvert tau ng js object, ung js object natin kinonsole log natin
// Display the fulfilled response frm the previous .then()

// Translating arrow function to traditional function
/*
	fetch("https://jsonplaceholder.typicode.com/posts")
	.then(function(res){ 
		return res.json()
	})
	.then(function(response){ 
		return console.log(response)
	})
*/

// [SECTION] Getting a specific post
// ":id" is a wildcard where you can put any value it then create a link between "id" parameter in the url and the value provided in the URL "https://jsonplaceholder.typicode.com/posts/1"
/*
fetch("https://jsonplaceholder.typicode.com/posts/1") // fetch this record then get the json response then convert it to js object then console log
.then(res => res.json())
.then(response => console.log(response))
*/

console.log("-------------------------------------------");
// The async and await keyword to achive asynchronous code. / it makes sure that every task is completed, depended si await kay async, so you cannot use await, without using async

async function fetchData(){
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
	console.log(result);

	let json = await result.json();
	console.log(json);

	console.log("Hello World");
}
fetchData()

console.log("-------------------------------------------");
// GET, POST, PUT, PATCH, DELETE

// [GET] getting a specific document
fetch("https://jsonplaceholder.typicode.com/posts/") 
.then(res => res.json())
.then(response => console.log(response))

// [POST] Inserting a documents/field
/*
	Syntax: 
		fetch("apiURL", {options})
		.then((response) => {})
		.then((response) => {})
*/		

fetch("https://jsonplaceholder.typicode.com/posts/", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hello World",
			userId: 1
		})
	}) 
	.then(response => response.json())
	.then(json => console.log(json));

// [PUT] updating a whole document (all fields)
/*
	Syntax: 
		fetch("apiURL", {options})
		.then((response) => {})
		.then((response) => {})
*/	
fetch("https://jsonplaceholder.typicode.com/posts/1",
	{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "updated post",
		body: "Hello again",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [PATCH] Updating a specific field of a document
// PUT vs PATCH
	// PATCH is used to update single/several properties
	// PUT is used to update the whole documents/object
fetch("https://jsonplaceholder.typicode.com/posts/1",
{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title"
	})
})
.then(response => response.json())
.then(json => console.log(json));

// [DELETE] Removing/deleting a document
fetch("https://jsonplaceholder.typicode.com/posts/1",
{
	method: "DELETE"
})
.then(response => response.json())
.then(json => console.log(json));

// Additional code: 

/*fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});
*/